global measurements
measurements = [
	struct(
	% centimeters
	'length', 78.5,
	'timings', [
	% seconds per 10 periods
	17.67,
	% another measurement of 10 periods
	17.64
	]
	),
	struct(
	'length', 65,
	'timings', [
	15.96,
	15.99
	]
	),
	struct(
	'length', 53.25,
	'timings', [
	14.45,
	14.54
	]
	),
	struct(
	'length', 40,
	'timings', [
	12.56,
	12.75
	]
	),
	struct(
	'length', 132,
	'timings', [
	23.11,
	22.93
	]
	),
	struct(
	'length', 119,
	'timings', [
	21.97,
	21.73
	]
	),
	struct(
	'length', 105.5,
	'timings', [
	20.6,
	20.49
	]
	),
	struct(
	'length', 93.7,
	'timings', [
	19.32,
	19.36
	]
	)
];
global measurements_errors_ranges
measurements_errors_ranges = struct(
	'length', 0.01,
	'timings', 0.01
);

% We are interested in this:
%
%      ⎛ T     ⎞        ⎛ l  ⎞
%   log⎜───────⎟ = α⋅log⎜────⎟
%      ⎝T(lₘₐₓ)⎠        ⎝lₘₐₓ⎠
%
% Where `log` is the natural logarithm
%
% So let's start by calculating lₘₐₓ and T(lₘₐₓ):
%
all_lengths = arrayfun( ...
	@(x) x.length, ...
	measurements ...
);
global l_max;
l_max = max(all_lengths);
% Take the index of l_max
global l_max_idx;
l_max_idx = [measurements.length] == l_max;
% Take the mean of the timings measured for that rope length
global T_l_max;
T_l_max = mean(measurements(l_max_idx).timings);

% Takes:
%
% 1) A struct with the fields 'length' and 'timings' as in 1 element of
% measurements.
%
% Returns a struct with fields:
%
% 'log_form') a 1x2 array of:
%
%   ⎡    ⎛ T     ⎞      ⎛ l  ⎞ ⎤
%   ⎢ log⎜───────⎟   log⎜────⎟ ⎥
%   ⎣    ⎝T(lₘₐₓ)⎠ ,    ⎝lₘₐₓ⎠ ⎦
%
%    Where `log` is the natural logarithm
%
% 'err') a 1x2 array of:
%
%   ⎡  error on x    error on y ⎤
%   ⎢    axis          axis     ⎥
%   ⎣              ,            ⎦
%
function handled = handle_single_measurement(m)
	% Bring those to scope
	global l_max;
	global l_max_idx;
	global T_l_max;
	global measurements_errors_ranges;
	% No need to convert to proper units as we are interested in the ratio
	% between (1) what's measured and (2) the maximum values.
	l = m.length;
	% We took 2 measurements for every certain length of rope so we
	% take the mean of both of them.
	T = mean(m.timings);
	handled = struct(
		'log_form', [ log(T/T_l_max), log(l/l_max) ],
		% Note that the ... is important, as otherwise the array gets different
		% dimensions
		'err', [ ...
			measurements_errors_ranges.timings / T, ...
			measurements_errors_ranges.length / l ...
		]
	);
end
function [linear_measurements, errs] = handle_measurements(measurements, errors_ranges)
	handled = cell2mat(arrayfun( ...
		@(x) handle_single_measurement(x), ...
		measurements, ...
		'UniformOutput', false ...
	));
	linear_measurements = cell2mat(arrayfun( ...
		@(x) x.log_form, ...
		handled, ...
		'UniformOutput', false ...
	));
	errs = cell2mat(arrayfun( ...
		@(x) x.err, ...
		handled, ...
		'UniformOutput', false ...
	));
end

[linear_measurements, errorbars_arr] = handle_measurements(measurements, measurements_errors_ranges);

x_data_unsorted = linear_measurements(:,2);
x_data = sort(x_data_unsorted);
y_data_unsorted = linear_measurements(:,1);
y_data = sort(y_data_unsorted);

%% NOTE: The errorbars are here - remove the semicolon to see their values when you run this.
x_err = errorbars_arr(:,2);
y_err = errorbars_arr(:,1);

% Get a polynomial (of degree 1) fit for the x and y data
[fit, regression_info] = polyfit(x_data, y_data, 1);
% Computing the ss_res according to wikipedia:
% https://en.wikipedia.org/wiki/Coefficient_of_determination#Comparison_with_norm_of_residuals
ss_res = regression_info.normr^2;
y_bar = mean(y_data);
ss_tot = sum(arrayfun(...
	@(y) (y - y_bar)^2, ...
	y_data ...
));
R2 = 1 - ss_res/ss_tot;

% Create a function with the a and b fit of the model:
%
%   y = a * x + b
%
% Where:
%
% - a == fit(1)
% - b == fit(2)
%
fit_expr = @(x) fit(1) * x + fit(2);
% Get a sane plot_domain for the x axis on which we'll plot fit_expr
x_min = x_data(1) - abs(x_data(1) - x_data(2))/2;
x_max = x_data(end) + abs(x_data(end) - x_data(end-1))/2;
plot_domain = [x_min, x_max];

% We expect `b` to be close to 0 and `alpha` to be close to 0.5
expected_alpha = 0.5;
expected_fit_expr = @(x) expected_alpha * x + 0;

% Print fancy stuff
disp("For the model:")
disp("")
disp("      ⎛ T     ⎞        ⎛ l  ⎞")
disp("   log⎜───────⎟ = α⋅log⎜────⎟")
disp("      ⎝T(lₘₐₓ)⎠        ⎝lₘₐₓ⎠")
disp("")
disp("The measurements gave:")
disp("")
disp(sprintf("  α == %d", fit(1)))
disp("")
disp("And the expectation was:")
disp("")
disp(sprintf("  α == %d", expected_alpha))
disp("")
disp(sprintf("So we are %d%% close", 100 - 100 * abs(expected_alpha - fit(1))))
disp("")
disp("See the graph for a visual representation of the results vs the expected results")
disp("")
disp("R^2 is:")
disp("")
disp(sprintf("%d", R2))

% % Plot stuff
figure;
hold on;
% Regression plot
fplot(fit_expr, plot_domain);
fplot(expected_fit_expr, plot_domain);
plot(x_data_unsorted, y_data_unsorted, 'b+');
xlabel('log(L/L_{max})')
ylabel('log(T/T_{max})')
legend({ ...
	sprintf("Measured: y = %d * x", fit(1)) ...
	sprintf("Expected: y = %d * x", expected_alpha), ...
	}, ...
	'Location', "north" ...
)
legend('boxoff')
errorbar(x_data_unsorted, y_data_unsorted, x_err, '>.');
errorbar(x_data_unsorted, y_data_unsorted, y_err, '#~.');

% Print it to a png
print -dpng "final-log-plot.png"

%% Now, for completness of the report, let's put here what period we got per angle

global ceiling_length_vs_period_measurements_const_length
% centimeters
ceiling_length_vs_period_measurements_const_length = 40;

global ceiling_length_vs_period_measurements
ceiling_length_vs_period_measurements = [
	struct(
	% The length from the mass to the ceiling, the real angle is given by the
	% arccos of: (this number) / ceiling_length_vs_period_measurements_const_length
	% This value is in centimeters
	'length', 28.5,
	% seconds per 10 periods
	'timing', 12.6
	),
	struct(
	'length', 25,
	'timing', 12.23
	),
	struct(
	'length', 33.25,
	'timing', 12.75
	),
	struct(
	'length', 32,
	'timing', 12.34
	),
	struct(
	'length', 19,
	'timing', 12.45
	),
	struct(
	'length', 15.5,
	'timing', 12.69
	),
	struct(
	'length', 12.3,
	'timing', 13.31
	)
];

angle_vs_period_measurements = cell2mat(arrayfun( ...
	@(x) struct( ...
		% compute the angle with an arccos
		'angle', acos( ...
			x.length/ ...
			% Constant
			ceiling_length_vs_period_measurements_const_length ...
		), ...
		'timing', x.timing ...
	), ...
	ceiling_length_vs_period_measurements, ...
	'UniformOutput', false ...
));

% Compute error bar for angle_vs_period_measurements. Do it with
% matlab's symbolic computation
syms x
% shgiaa nigreret function
diff_F = abs( ...
	% Derive
	diff( ...
		acos(x/ceiling_length_vs_period_measurements_const_length) ...
	) * measurements_errors_ranges.length ...
);

errorbars_arr = cell2mat(arrayfun( ...
	@(v) [double(subs(diff_F, x, v.angle)), measurements_errors_ranges.timings/10 ], ...
	angle_vs_period_measurements, ...
	'UniformOutput', false ...
));

angle_vs_period_measurements_table = cell2mat(arrayfun( ...
	@(v) [v.angle, v.timing/10], ...
	angle_vs_period_measurements, ...
	'UniformOutput', false ...
));

x_data_unsorted = angle_vs_period_measurements_table(:,1);
y_data_unsorted = angle_vs_period_measurements_table(:,2);

x_data = sort(x_data_unsorted);
y_data = sort(y_data_unsorted);

x_err = errorbars_arr(:,2);
y_err = errorbars_arr(:,1);

% Get a polynomial (of degree 1) fit for the x and y data
[fit, regression_info] = polyfit(x_data, y_data, 1);

% Create a function with the a and b fit of the model:
%
%   y = a * x + b
%
% Where:
%
% - a == fit(1)
% - b == fit(2)
%
fit_expr = @(x) fit(1) * x + fit(2);
% Get a sane plot_domain for the x axis on which we'll plot fit_expr
x_min = x_data(1) - abs(x_data(1) - x_data(2))/2;
x_max = x_data(end) + abs(x_data(end) - x_data(end-1))/2;
plot_domain = [x_min, x_max];

% Clear the previous plot
clf('reset')
hold on;
% Regression plot
fplot(fit_expr, plot_domain);
plot(x_data_unsorted, y_data_unsorted, 'b+');
ylabel('Time Period [s]');
xlabel('Angle (radians)');
ylim([0 4])
legend({ ...
		sprintf("Measured: y = %d * x", fit(1)) ...
	}, ...
	'Location', "north" ...
)
legend('boxoff')
errorbar(x_data_unsorted, y_data_unsorted, x_err, '>.');
errorbar(x_data_unsorted, y_data_unsorted, y_err, '#~.');

% Print it to a png
print -dpng "angle-vs-period.png"
