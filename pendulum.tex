%! TEX program = xelatex
\documentclass[a4paper]{article}

\usepackage{amsmath}
\usepackage{siunitx}
% See https://tex.stackexchange.com/a/82462/125609
\usepackage{caption}
\captionsetup[figure]{labelformat=empty}% redefines the caption setup of the figures environment in the beamer class.
\usepackage{titling}

% Links
\usepackage{hyperref}
\hypersetup{colorlinks = true,
	citecolor = gray,
	linkcolor = red,
	citecolor = green,
	filecolor = magenta,
	urlcolor = cyan
}
% Drawings and figures
\usepackage[american,siunitx]{circuitikz}
\usepackage{graphicx}
\usepackage{listings}
\lstset{basicstyle=\footnotesize,
	tabsize=4,
	keywordstyle=\color{blue},
	stringstyle=\color{red},
	commentstyle=\color{green},
	morecomment=[l][\color{magenta}],
}

%% the following commands are needed for some matlab2tikz features
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usetikzlibrary{plotmarks}
\usetikzlibrary{arrows.meta}
\usepgfplotslibrary{patchplots}
\usepackage{grffile}
\usetikzlibrary{calc,patterns,angles,quotes}

% Looks
\usepackage[top=1in,bottom=1in,inner=3cm,outer=2cm]{geometry}
% Provides commands to disable pagebreaking within a given vertical space. If
% there is not enough space between the command and the bottom of the page, a
% new page will be started.
\usepackage{needspace}
% For tables
\usepackage{booktabs}
\usepackage{color}
\usepackage{mathtools}
% Remove section and chapters numbers from headings -
% https://tex.stackexchange.com/questions/297265/remove-sections-numbering
\setcounter{secnumdepth}{0}

% Language
\usepackage{polyglossia}
\setdefaultlanguage{hebrew}
\setotherlanguage{english}
\usepackage{hebrewcal}

% Fonts
\setmainfont{DejaVu Serif}
\setsansfont{DejaVu Sans}
\setmonofont{DejaVu Sans Mono}
\newfontfamily\hebrewfont{Frank Ruehl CLM}[Script=Hebrew]
\newfontfamily\hebrewfontsf{FreeSerif}[Script=Hebrew]
\newfontfamily\hebrewfonttt{Liberation Mono}[Script=Hebrew]

% Fix for a parentheses issue with hebrew fonts + polyglossia, see
% https://tex.stackexchange.com/a/141437/125609
\makeatletter
\def\maketag@@@#1{\hbox{\m@th\normalfont\LRE{#1}}}
\def\tagform@#1{\maketag@@@{(\ignorespaces#1\unskip)}}
\makeatother

\begin{document}

\title{
	מעבדת פיזיקה 1מ \\
	התלות בין תדירות המטוטלת לאורכה
}
\author{\\
	דורון בכר \\
	ת.ז 316097872 \\
}


\begin{titlingpage}
\maketitle
\begin{abstract}
הרכבנו מטוטלת אשר בקירוב טוב מקיימת את הנחות המודל של
מטוטלת מתמטית. מדדנו את זמן המחזור של המטוטלת עבור אורכים שונים.
מצאנו מהמדידות את הקשר בין אורך החוט ותדירות המטוטלת / זמן המחזור.
השווינו את הקשר שמצאנו עם הקשר של המודל.
\end{abstract}
\end{titlingpage}


\section{מבוא}

בניסוי זה, רצינו לבדוק את המודל הפיזיקלי לקשר בין תדירות מטוטלת
לאורך החוט שלה.

נדגים איך מתקבל המודל התיאורטי:

\hspace{5 cm}

\begin{minipage}{0.6\textwidth}

\subsection{סימונים:}

\begin{list}{$\circ$}{}
\item $\mathbf{T} [N]$ - המתיחות בחוט
\item $L [m]$ - אורך החוט.
    \item $g [\frac{m}{s^2}]$ - גודל תאוצת הכובד.
    \item $\theta = \theta(t)$ - הזווית בין החוט לכיוון תאוצת הכובד $\mathbf{g}$.
    \item $a [\frac{m}{s^2}]$ - גודל תאוצת המסה.
    \item $m [kg]$ - המסה.
\end{list}

\end{minipage}
\begin{minipage}{0.3\textwidth}\raggedleft
\begin{tikzpicture}
    % save length of g-vector and theta to macros
    \pgfmathsetmacro{\Gvec}{1.5}
    \pgfmathsetmacro{\myAngle}{30}
    % calculate lengths of vector components
    \pgfmathsetmacro{\Gcos}{\Gvec*cos(\myAngle)}
    \pgfmathsetmacro{\Gsin}{\Gvec*sin(\myAngle)}

    \coordinate (centro) at (0,0);
    \draw[dashed,gray,-] (centro) -- ++ (0,-3.5) node (mary) [black,below]{$ $};
    \draw[thick] (centro) -- ++(270+\myAngle:3) coordinate (bob)
      coordinate (gcos)
      node[midway,right] {$L$};
    \pic [draw, ->, "$\theta$", angle eccentricity=1.5] {angle = mary--centro--bob};
    \draw [blue,-stealth] (bob) -- ($(bob)!\Gcos cm!(centro)$)
      coordinate (gcos)
      node[near end, left] {$T$};
      \draw [red,-stealth] ($(bob) + (1,1)$) -- ($(bob)!\Gcos cm!(centro) + (1,1)$)
      coordinate (gcos)
      node[near end, left] {$y$};
    \draw [-stealth] (bob) -- ($(bob)!-\Gcos cm!(centro)$)
      coordinate (gcos)
      node[midway,above right] {$mgcos\theta$};
    \draw [-stealth] (bob) -- ($(bob)!\Gsin cm!90:(centro)$)
      coordinate (gsin)
      node[midway,above left] {$mgsin\theta$};
      \draw [red,-stealth] ($(bob) + (1,1)$) -- ($(bob)!\Gsin cm!90:(centro) + (1,1)$)
      coordinate (gsin)
      node[midway,above left] {$x$};
    \draw [-stealth] (bob) -- ++(0,-\Gvec)
      coordinate (g)
      node[near end,left] {$mg$};
    \pic [draw, ->, "$\theta$", angle eccentricity=1.5] {angle = g--bob--gcos};
    \filldraw [fill=black!40,draw=black] (bob) circle[radius=0.1];
\end{tikzpicture}
\end{minipage}

\hspace{5 cm}

\subsubsection{תוצאות המודל התיאורטי}

כידוע, אם הזוויות קטנות, על פי קירוב \textenglish{Taylor}, ($sin\theta \simeq
\theta $) תדירות התנודות היא:

\begin{equation}
    T = \sqrt{\frac{g}{L}}
\end{equation}

בשביל שיהיה לנו קל להוכיח את הקשר הנ"ל, רצינו "להמיר" אותו לקשר לינארי. עשינו
זאת באמצעות:

\begin{equation} \label{eq:log}
    \boxed{ln(\frac{T}{T(L_{max})}) = \alpha \cdot ln(\frac{L}{L_{max}})}
\end{equation}

כאשר:

\begin{list}{$\circ$}{}
    \item $\alpha = 0.5$.
    \item $L_{max}$ הוא האורך המקסימלי שנמדוד.
    \item $T(L_{max})$ הוא זמן המחזור שנמדוד עבור $L_{max}$.
\end{list}

\subsection{אופן המדידה}

השתמשנו במטר עם דיוק של $0.01 [m]$, שעון עצר בפלאפון עם דיוק של $0.01 [s]$
ומדדנו 10 זמני מחזור לכל אורך חוט ותלינו משקולת יחסית כבדה ו"נקודתית" על חוט עם
משקל קטן יחסית אליה.

הניסוי בוצע בשעת ערב בעת שהרוח הייתה חלשה. דאגנו לוודא זאת כדאי שנוכל להיות
בטוחים שהתנועה של המסה נשארת במישור אחד. מדדנו את השפעת הרוח באמצעות דף נייר
שהוחזק בזויות שונות וראינו שהכיוון שלה מקביל לתנועת המטוטלת והעוצמה שלה מרימה
את קצה הנייר בפחות מ-$0.50 \pm 0.01 [m]$.

\includegraphics[width=0.48\textwidth]{setup1.jpg}
\includegraphics[width=0.48\textwidth]{setup2.jpg}

\section{תוצאות הניסוי}

בחרנו 8 אורכים שונים למטוטלת. לכל אורך, מדדנו את הזמן שלקח ל-10 זמני מחזור
להתרחש, פעמיים. סך הכל קיבלנו 2 מדידות לכל אורך, סך הכל 16 מדידות. לפני שהתחלנו
לעבד את הנתונים, חישבנו את הממוצע של כל זוג מדידות כזה והתייחסנו בשאר התוכנית
לממוצע זה כאל זמן המחזור.

בשלב זה רצינו ליצור 2 מערכים שיתאימו לקשר הלינארי שפיתחנו במשוואה~\ref{eq:log}.
לכן מצאנו קודם את האורך המקסימלי שמדדנו ($L_{max}$) ועבור מדידה זו לקחנו את זמן
המחזור ($T(L_{max})$).

עם שני משתנים אלו, חישבנו את המערך שיתאים לציר ה-$x$ כלומר לכל אורך, חישבנו:

$$ ln(\frac{L}{L_{max}}) $$

ואת המערך שיתאים לציר $y$, כלומר לכל זמן מחזור חישבנו:

$$ ln(\frac{T}{T(L_{max})}) $$

לאחר מכן, השתמשנו בפונקציה
\href{https://octave.org/doc/v5.2.0/Polynomial-Interpolation.html#index-polyfit}{\texttt{polyfit}}
כדי להתאים את ערכי ה-$x$ לערכי ה-$y$ עבור פולינום מסדר ראשון. ציירנו את
הפונקציה ש-\texttt{polyfit} החזיר לנו לעומת הנקודות שקיבלנו, יחד עם הפונקציה
שציפינו לקבל שהיא כמובן $y = 0.5 \cdot x$. קיבלנו את הגרף הבא:

\begin{figure}[h]
\includegraphics[width=0.9\textwidth]{final-log-plot.png}
\caption{}
\label{fig:log_plot}
\end{figure}

\section{דיון בתוצאות}

\subsection{השוואה עם התיאוריה}

קיבלנו שאכן ישנו קשר לינארי בין ערכי ה-$x$ ל-$y$ כפי שצפינו. ההתאמה של הנקודות
לגרף התקבל עם שגיאה של $R^2 == 0.999708$. כפי שנראה בגרף, השיפוע שהתקבל להתאמה
קרוב עד כדי $99.5123\%$ לתוצאה הרצויה.

המודל התיאורטי מבוסס על פתרון משוואת כוחות באמצעות קירוב \textenglish{Taylor}
סביב $\theta = 0$. לכן, הוא לא באמת נכון, והוא רק קירוב של הפתרון האמיתי עבור
זוויות קטנות. אף על פי כן, כפי שניכר בתוצאות, זהו קירוב לא רע גם עבור זוויות
בסדר גודל של עד $\ang{20}$. בשביל לאמת את התצפית הזו, בנינו גם גרף של זמן
המחזור כפונקציה של הזווית, וראינו ששיפועו כמעט אפס:

\clearpage

\begin{figure}[h]
\includegraphics[width=0.8\textwidth]{angle-vs-period.png}
\caption{}
\label{fig:T_angle}
\end{figure}

לכן בחרנו לעבוד עם זווית התחלתית של $\ang{20}$ בשאר המדידות כדאי שגם נוכל לראות
בבירור את התנועה וגם שלא נסטה יותר מידי מהקירוב של המודל.

בנוסף לקירוב \textenglish{Taylor}, סטייה נוספת של הניסוי מהתיאוריה נובעת
מהעובדה שהמסה שבחרנו אינה נקודתית. בשביל למזער את שגיאה זו, בחרנו במסה במשקל
$3.000 \pm 0.001[kg]$ שעשויה מברזל מלא וכן נפחה בהתאם למשקל הסגולי של ברזל שהוא
$7834 [kg/m^3]$.
\footnote{\url{https://www.wolframalpha.com/input/?i=density+of+iron}}

מקור סטייה נוסף מהמודל הינו העובדה שביצענו את הניסוי במקום עם רוח קלילה וזאת
בנוסף לחיכוך עם האוויר. בשביל לאמוד את השפעת האוויר על תנועת המטוטלת, מדדנו כמה
מחזורים לוקחים עד שהמטוטלת מאבדת חצי מהאמפליטודה שלה, (עבור שחרור מזווית
$\ang{20}$) קיבלנו שלקח לאמפליטודה 60 מחזורים לדעוך.

החוט שבחרנו גם כן תורם לסטייה של הניסוי מהמודל. במודל לחוט לא אמורה להיות מסה,
ואילו אצלנו יש לו מסה של $ 0.1 [kg]$. משקל זה זניח לעומת משקלה של המשקולת לכן
זה מספיק טוב בשביל להתקרב למודל.

\subsection{שגיאות המדידה}

מכיוון שהמדידות לא היו מושלמות, יש צורך לחשב ולהראות על הגרף את הפער האפשרי בין
המדידות לערך האמיתי. בשביל לחשב את גודל שגיאה זו כך שיתאים למימדים של הגרף,
ובגלל שהפעלנו פונקצית $ln()$ על המדידות, קיבלנו (בהתאם לכלי המדידה שלנו) שגיאה
מחושבת בסדר גודל של $0.005$ בציר ה-$y$ ושל $0.0001$ בציר ה-$x$.  \footnote{אפשר
לראות את ערכי השגיאה בתוכנית
\href{https://gitlab.com/doronbehar/physics1m-lab-pendulum/blob/c94db1f1e1ce15ff298dc2058140a61ac1a07ea3/pendulum.m\#L160-162}{כאן}.}

חוץ מאי דיוק של מכשירי המדידה בהם התחשבנו בחישובים המתוארים לעיל ושמוצגים על
הגרף (גרף~\ref{fig:log_plot}), מפעילי הניסוי יכולים לתרום לאי דיוקים במדידה של
אורך החוט ושל זמן המחזור. זאת בגלל קריאה לא מושלמת של השנתות על המטר, וזמן
תגובה שאינו $0 [s]$ בלחיצה על שעון העצר. בשביל למזער עד כמה שאפשר את אי הדיוק
שמקורו בזמן תגובה, הסתכלנו על המטוטלת בזמן בה היא במהירות הכי גבוה - למטה, כדאי
שיהיה יותר קל לזהות בדיוק מתי מתחיל ונגמר מחזור.

\section{מסקנות}

יש אכן קשר של $ T = \beta \cdot \sqrt{L} $ בין זמן המחזור של מטוטלת לאורך החוט
שלה, כאשר $\beta$ הוא איזשהו קבוע שאינו תלוי ב-$L$. זה נובע ישירות מהמשוואה
הלינארית שפותחה במבוא - משוואה~\ref{eq:log}.

\section{נספח}

הקוד שנכתב לצורך עיבוד הנתונים, חישוב השגיאות, והנתונים עצמם נמצאים
\href{https://gitlab.com/doronbehar/physics1m-lab-pendulum/blob/master/pendulum.m}{כאן}.

\end{document}
