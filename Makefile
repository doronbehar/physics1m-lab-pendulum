pendulum.pdf: pendulum.tex final-log-plot.png
	tectonic pendulum.tex

final-log-plot.png: pendulum.m
	octave pendulum.m

all: pendulum.pdf

open: pendulum.pdf
	gio open pendulum.pdf

.PHONY: open
